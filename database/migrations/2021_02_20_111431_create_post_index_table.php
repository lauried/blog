<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostIndexTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_index', function (Blueprint $table) {
            $table->foreignId('post_id');
            $table->string('word');
            $table->integer('count');

            $table->primary([ 'post_id', 'word' ]);
            $table->unique([ 'post_id', 'word' ]);
            $table->index('word');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_index');
    }
}
