# Blog

A blog project in which I learn the basics of Laravel.

I want to keep the scope of this project fairly small. At the same time I want to combine the basic features of a blog with the cross-linkage of a wiki, to give it a bit of extra flexibility. The design isn't fully done yet because I'm just getting settled in using the framework.
