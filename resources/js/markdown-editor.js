/**
 * App-specific usage of a markdown editor.
 * The choice of underlying editor is an implementation detail.
 */

window.SimpleMDE = require('simplemde');

$(document).ready(function () {
    $('.markdown-editor').each(function () {

        function insertPostLink(editor) {
            let cm = editor.codemirror;
            let timer;

            function onCancel() {
                clearTimeout(timer);
            }

            function onAccept() {
            }

            let body = modalShow('Link Another Post', '', '', onAccept, onCancel);
            body.append(
                '<small>Start typing an article name, and click the article to insert a link to it.</small>'
                + '<input class="form-control" type="text" placeholder="search" id="post-link-search">'
                + '<ul class="list-group" id="results"></ul>'
            );

            let search = body.find('#post-link-search');
            let results = body.find('#results');
            let oldSearch = search.val();
            function searchPoll() {
                if (search.val() == oldSearch) {
                    timer = setTimeout(searchPoll, 100);
                    return;
                }

                oldSearch = search.val();
                $.ajax({
                    url: '/ajax/autocomplete-post',
                    data: { query: oldSearch },
                    complete: function () {
                        timer = setTimeout(searchPoll, 100);
                    },
                    success: function (data, textStatus, jqXHR) {
                        results.html('');
                        $.each(data, function (index, item) {
                            let post = $('<li class="list-group-item">' + item.title + '</li>');
                            post.on('click', function () {
                                cm.replaceSelection('[' + item.title + '](/post/' + item.uri + ')');
                                modalClose();
                            });
                            results.append(post);
                        });
                    }
                });
            }
            timer = setTimeout(searchPoll, 100);
        }

        var toolbar = [
            // We have to manually add all the icons we want, but okay.
            {
                name: "bold",
                action: SimpleMDE.toggleBold,
                className: "fa fa-bold",
                title: "Bold",
                default: true
            },
            {
                name: "italic",
                action: SimpleMDE.toggleItalic,
                className: "fa fa-italic",
                title: "Italic",
                default: true
            },
            {
                name: "strikethrough",
                action: SimpleMDE.toggleStrikethrough,
                className: "fa fa-strikethrough",
                title: "Strikethrough"
            },
            {
                name: "heading-smaller",
                action: SimpleMDE.toggleHeadingSmaller,
                className: "fa fa-header fa-header-x fa-header-smaller",
                title: "Smaller Heading"
            },
            {
                name: "heading-bigger",
                action: SimpleMDE.toggleHeadingBigger,
                className: "fa fa-header fa-header-x fa-header-bigger",
                title: "Bigger Heading"
            },

            '|',

            {
                name: "code",
                action: SimpleMDE.toggleCodeBlock,
                className: "fa fa-code",
                title: "Code"
            },
            {
                name: "quote",
                action: SimpleMDE.toggleBlockquote,
                className: "fa fa-quote-left",
                title: "Quote",
                default: true
            },
            {
                name: "unordered-list",
                action: SimpleMDE.toggleUnorderedList,
                className: "fa fa-list-ul",
                title: "Generic List",
                default: true
            },
            {
                name: "ordered-list",
                action: SimpleMDE.toggleOrderedList,
                className: "fa fa-list-ol",
                title: "Numbered List",
                default: true
            },
            {
                name: "table",
                action: SimpleMDE.drawTable,
                className: "fa fa-table",
                title: "Insert Table"
            },

            '|',

            {
                name: "link",
                action: SimpleMDE.drawLink,
                className: "fa fa-link",
                title: "Create Link",
                default: true
            },
            {
                name: "image",
                action: SimpleMDE.drawImage,
                className: "fa fa-picture-o",
                title: "Insert Image",
                default: true
            },
            {
                name: "insertPostLink",
                action: insertPostLink,
                className: "fa fa-anchor",
                title: "Link to a post",
            },

            '|',

            {
                name: "preview",
                action: SimpleMDE.togglePreview,
                className: "fa fa-eye no-disable",
                title: "Toggle Preview",
                default: true
            },
            {
                name: "side-by-side",
                action: SimpleMDE.toggleSideBySide,
                className: "fa fa-columns no-disable no-mobile",
                title: "Toggle Side by Side",
                default: true
            },
            {
                name: "fullscreen",
                action: SimpleMDE.toggleFullScreen,
                className: "fa fa-arrows-alt no-disable no-mobile",
                title: "Toggle Fullscreen",
                default: true
            },

            '|',

            {
                name: "undo",
                action: SimpleMDE.undo,
                className: "fa fa-undo no-disable",
                title: "Undo"
            },
            {
                name: "redo",
                action: SimpleMDE.redo,
                className: "fa fa-repeat no-disable",
                title: "Redo"
            },

            '|',

            {
                name: "guide",
                action: "https://simplemde.com/markdown-guide",
                className: "fa fa-question-circle",
                title: "Markdown Guide",
                default: true
            }
        ];

        var editor = new SimpleMDE({
            element: $(this)[0],
            toolbar: toolbar
        });
    });
});