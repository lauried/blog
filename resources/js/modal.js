/**
 * Methods for manipulating the modal.
 */
window.modalShow = function(title, accept, cancel, onAccept, onCancel) {
    var ele = $('#the-modal');
    var primary = ele.find('.btn-primary');
    var secondary = ele.find('.btn-secondary');

    ele.find('.modal-title').text(title);
    if (accept) {
        primary.text(accept).off('click').on('click', onAccept).show();
    } else {
        primary.hide();
    }
    if (cancel) {
        secondary.text(cancel).show();
    } else {
        secondary.hide();
    }

    ele.off('hidden.bs.modal').on('hidden.bs.modal', onCancel);

    ele.modal();
    return ele.find('.modal-body').html('');
};

window.modalClose = function () {
    $('#the-modal').modal('hide');
};