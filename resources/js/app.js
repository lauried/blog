/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.$ = window.jQuery = require('jquery');
require('./bootstrap');
require('./modal.js');
require('./markdown-editor.js');
require('./jquery.cookie');

window.DataTables = require('datatables.net');
window.DataTablesEditor = require('datatables.net-editor');

require('jquery-ui/ui/widgets/autocomplete.js');
require('@accursoft/jquery-caret/jquery.caret');
require('jquery-tageditor/jquery.tag-editor.js');

$(document).ready(function () {
    $('#admin-view-level').on('change', function () {
        $.cookie('admin-view-level', $(this).val());
        location.reload();
    });

    $('.tag-list').tagEditor({
        autocomplete: {
            delay: 100,
            position: { collision: 'flip' },
            source: '/ajax/autocomplete-tag',
            minLength: 1
        }
    });
});
