@extends('layouts.blog')

@section('title')
    Search
@endsection

@section('content')
    <div class="search">
        <form method="get" action="">
            <div class="input-group">
                <input class="form-control" type="text" name="q" value="{{ $query }}">
                <input class="btn btn-primary" type="submit" value="Search">
            </div>
        </form>
    </div>
    @include('partials.search-results', [
        'posts' => $posts,
        'offset' => $offset,
        'count' => $count,
        'url' => $url,
        'time' => $time,
    ])
@endsection

@section('content-related')
@endsection