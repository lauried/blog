@extends('layouts.blog')

@section('title')
    Search
@endsection

@section('content')
    <h2>Tags</h2>
    <p>All tags from all articles.</p>
    <div class="tags-list">
        <ul>
            @foreach ($tags as $tag)
                <li><a href="/tag/{{ $tag }}">{{ $tag }}</a></li>
            @endforeach
        </ul>
    </div>
@endsection
