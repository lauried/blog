<ul>
    @foreach ($posts as $post)
        <li class="post-{{ $post->state }}">
            <span class="post-state"></span>
            <a class="post-link" href="/post/{{ $post->uri }}">{{ $post->title }}</a>
        </li>
    @endforeach
</ul>