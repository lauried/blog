<div class="posts">
    <p>Showing {{ $offset + 1 }} - {{ $offset + $count }}</p>
    <ul>
        @foreach ($posts as $index => $post)
            <li class="post-{{ $post->state }}">
                <span class="post-index">{{
                    isset($time) && $time === 'modified'
                        ? $post->updated_at->format('Y-m-d H:i:s')
                        : $post->created_at->format('Y-m-d H:i:s')
                }}: </span>
                <span class="post-state"></span>
                <a class="post-link" href="/post/{{ $post->uri }}">{{ $post->title }}</a>
            </li>
        @endforeach
    </ul>
    @if ($offset > 0)
        <a href="{{ str_replace([ '%O', '%C' ], [ $offset - $count, $count ], $url) }}">Previous</a>
    @endif
    @if (count($posts) == $count)
        <a href="{{ str_replace([ '%O', '%C' ], [ $offset + $count, $count ], $url) }}">Next</a>
    @endif
</div>