<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name', 'Blog') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <!-- The actual fonts are done in the CSS -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="main-wrapper">
        <div id="header">
            <section id="heading">
                <h1><a href="{{ config('app.url') }}/">{{ config('app.name', 'Blog') }}</a></h1>
                @auth()
                    <div class="form-group" id="admin-section">
                        <form method="post" action="{{ config('app.url') }}/logout">
                            @csrf
                            <input class="btn btn-sm btn-primary" type="submit" value="Log Out">
                        </form>
                        <label>
                            <select class="custom-select custom-select-sm" id="admin-view-level">
                                <option
                                    value="{{ \App\Post::ACTIVE }}"
                                    @if (Cookie::get('admin-view-level') === \App\Post::ACTIVE) selected="selected"@endif
                                >Show Published</option>
                                <option
                                    value="{{ \App\Post::HIDDEN }}"
                                    @if (Cookie::get('admin-view-level') === \App\Post::HIDDEN) selected="selected"@endif
                                >Show Unpublished</option>
                                <option
                                    value="{{ \App\Post::DELETED }}"
                                    @if (Cookie::get('admin-view-level') === \App\Post::DELETED) selected="selected"@endif
                                >Show Deleted</option>
                            </select>
                        </label>
                        <a class="btn btn-sm btn-primary" href="/edit-post">Create Post</a>
                        @yield('content-admin')
                    </div>
                @endauth
            </section>
        </div>
        <div id="inner-wrapper">
            <section id="content">
                @yield('content')
            </section>
            <section id="related">
                @yield('content-related')
            </section>
        </div>
        <div id="sidebar">
            <section>
                @if (config('app.guest_mode') || \Auth::user())
                <nav class="post-link-list">
                    <h2>Recent</h2>
                    @include('partials.post-list', [ 'posts' => $recentPosts ])
                </nav>
                <nav class="post-link-list">
                    <ul>
                        <li><a href="/recent/0/50">New Pages</a></li>
                        <li><a href="/modified">New Changes</a></li>
                        <li><a href="/search">Search</a></li>
                        <li><a href="/tags">Tags</a></li>
                    </ul>
                </nav>
                @endif
            </section>
            @include('partials.modal')
        </div>
        <div class="clear"></div>
    </div>
    <div id="login-thing"><a href="/login" rel="nofollow">#</a></div>
</body>
</html>
