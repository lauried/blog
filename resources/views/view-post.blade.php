@extends('layouts.blog')

@section('title')
    {{ $post->title }}
@endsection

@section('content-admin')
    @auth
    <a class="btn btn-sm btn-primary" href="/edit-post?post-id={{ $post->id }}&return-uri={{ url()->current() }}">Edit Post</a>
    @endauth
@endsection

@section('content')
    <div class="post post-{{ $post->state }}">
        <h2>{{ $post->title }}</h2>
        <p><span class="post-state"></span><span class="post-date">{{ $post->created_at->format('g:i a D j M Y') }}</span></p>
        @markdown($post->text->content)
        <p><span class="post-date">Last modified: {{ $post->updated_at->format('g:i a D j M Y') }}</span></p>
    </div>
@endsection

@section('content-related')
    @if ($post->tags->isNotEmpty())
        <nav class="post-link-list">
            <h3>Tags</h3>
            <ul>
                @foreach ($post->tags as $tag)
                    <li><a href="/tag/{{ $tag->name }}">{{ $tag->name }}</a></li>
                @endforeach
            </ul>
        </nav>
    @endif
    @if ($links->isNotEmpty())
        <nav class="post-link-list">
            <h3>Links from here</h3>
            <ul>
                @foreach ($links as $link)
                    <li><a href="/post/{{ $link->uri }}">{{ $link->title }}</a></li>
                @endforeach
            </ul>
        </nav>
    @endif
    @if ($linkingPosts->isNotEmpty())
        <nav class="post-link-list">
            <h3>Links to here</h3>
            <ul>
                @foreach ($linkingPosts as $link)
                    <li><a href="/post/{{ $link->uri }}">{{ $link->title }}</a></li>
                @endforeach
            </ul>
        </nav>
    @endif
@endsection