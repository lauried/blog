@extends('layouts.blog')

@section('title')
    {{ $title }}
@endsection

@section('content')
    <h2>{{ $title }}</h2>
    @include('partials.search-results', [
        'posts' => $posts,
        'offset' => $offset,
        'count' => $count,
        'url' => $url,
        'time' => $time,
    ])
@endsection
