@extends('layouts.blog')

@section('title')
    Edit Post
@endsection

@section('content')
    <section>
        <form method="POST" action="/edit-post">
            @csrf
            <input type="hidden" name="post-id" value="{{ $postId }}">
            <input type="hidden" name="return-uri" value="{{ $returnUri }}">
            <div class="form-group">
                <label for="edit-title">Title</label>
                <input type="text"
                       class="form-control"
                       id="edit-title"
                       name="edit-title"
                       aria-describedby="edit-title-help"
                       value="{{ $editTitle }}"
                >
                <small id="edit-title-help" class="form-text text-muted">
                    Title of the post. The URI is generated from the title, and it's displayed as the primary heading
                    when reading posts.
                </small>
                @error('edit-title')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="edit-content">Content</label>
                <textarea class="form-control markdown-editor"
                          id="edit-content"
                          name="edit-content"
                          aria-describedby="edit-content-help"
                >{{ $editContent }}</textarea>
                <small id="edit-content-help" class="form-text text-muted">
                    The post content, as Markdown.
                </small>
                @error('edit-content')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="edit-tags">Tags</label>
                <input type="text"
                       class="form-control tag-list"
                       id="edit-tags"
                       name="edit-tags"
                       aria-describedby="edit-tags-help"
                       value="{{ $tags }}"
                >
                <small id="edit-tags-help" class="form-text text-muted">
                    Extra tags by which the post can be searched. They also help to categorise a post.
                </small>
                @error('edit-tags')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <div class="form-check">
                    <input type="checkbox"
                           class="form-check-input"
                           id="edit-private"
                           name="edit-private"
                           value="1"{{ $private ? ' checked' : '' }}>
                    <label class="form-check-label" for="edit-private">Private page</label>
                </div>
                <div class="form-check">
                    <input type="checkbox"
                           class="form-check-input"
                           id="edit-nohistory"
                           name="edit-nohistory"
                           value="1"{{ $noHistory ? ' checked' : '' }}>
                    <label class="form-check-label" for="edit-nohistory">Don't show in recent</label>
                </div>
                <div class="form-check">
                    <input type="checkbox"
                           class="form-check-input"
                           id="edit-homepage"
                           name="edit-homepage"
                           value="1"{{ $homepage ? ' checked' : '' }}>
                    <label class="form-check-label" for="edit-homepage">Show on homepage</label>
                </div>
            </div>
            <div class="form-group">
                <label for="edit-status">Status</label>
                <select class="form-control" id="edit-state" name="edit-state">
                    <option value="active"{{ $state == \App\Post::ACTIVE ? ' selected' : '' }}>Published</option>
                    <option value="hidden"{{ $state == \App\Post::HIDDEN ? ' selected' : '' }}>Not Published</option>
                    <option value="deleted"{{ $state == \App\Post::DELETED ? ' selected' : '' }}>Deleted</option>
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-secondary" href="{{ $returnUri }}">Cancel</a>
            </div>
        </form>
    </section>
@endsection