<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('index'); })->name('index');

// Get the required auth routes but disable registration and so on.
Auth::routes([
    'passwords/confirm' => false,
    'passwords/email' => false,
    'passwords/reset' => false,
    'register' => false,
    'verify' => false,
]);

Route::get('/', 'ContentController@showHomepage');
Route::get('/post/{id}', 'ContentController@showPost');
Route::get('/tag/{tag}', 'ContentController@showByTag');
Route::get('/tags', 'ContentController@listTags');
Route::get('/recent/{offset}/{count}', 'ContentController@showRecent');
Route::get('/modified', 'ContentController@showModified');
Route::get('/search', 'ContentController@showSearch');

Route::get('/edit-post', 'PostEditController@showEditForm');
Route::post('/edit-post', 'PostEditController@handleEditForm');

Route::get('/ajax/autocomplete-post', 'AjaxController@autocompletePost');
Route::get('/ajax/autocomplete-tag', 'AjaxController@autocompleteTag');
