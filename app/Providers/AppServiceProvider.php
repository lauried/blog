<?php

namespace App\Providers;

use App\Post;
use App\Services\PostService;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.blog', function ($view) {
            $postService = resolve(PostService::class);
            $user = User::find(Auth::id());
            $level = Post::ACTIVE;
            if ($user !== null) {
                $level = Cookie::get('admin-view-level');
            }
            $view->with('recentPosts', $postService->getRecent($user, 10, 0, Post::ORDER_CREATED, $level));
        });
    }
}
