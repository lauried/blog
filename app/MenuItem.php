<?php

namespace App;

class MenuItem implements \JsonSerializable
{
    /** @var string */
    const URL = 'url';

    /** @var string */
    const POST_ID = 'post_id';

    /** @var string[] */
    const VALID_TYPES = [
        self::URL,
        self::POST_ID,
    ];

    /** @var string */
    private $name;

    /** @var string */
    private $type;

    /** @var string */
    private $value;

    /**
     * @param array|null $json
     */
    public function __construct(array $json = null)
    {
        if ($json !== null) {
            $this->setName($json['name'] ?? '');
            $this->setValue($json['value'] ?? '', $json['type'] ?? '');
        }
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        if (empty($name)) {
            throw new \InvalidArgumentException('name must not be empty');
        }

        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $value
     * @param string $type
     */
    public function setValue(string $value, string $type): void
    {
        switch ($type) {
            case self::URL:
                if (!filter_var($value, FILTER_VALIDATE_URL)) {
                    throw new \DomainException('value must be a URL for type "url"');
                }
                break;
            case self::POST_ID:
                if (!ctype_digit($value) || empty($value)) {
                    throw new \DomainException('value must represent an integer for type "post_id"');
                }
                break;
            default:
                throw new \DomainException('type must be one of "', implode('", "', self::VALID_TYPES) . "'");
        }

        $this->type = $type;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'type' => $this->type,
            'value' => $this->value,
        ];
    }
}
