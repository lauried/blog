<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostTag extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function post()
    {
        return $this->belongsTo('App\Post', 'post_id');
    }
}
