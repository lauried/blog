<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostIndex extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'post_index';

    public function post()
    {
        return $this->hasOne('App\Post', 'id', 'post_id');
    }
}
