<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class UserCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {name} {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     *
     * @todo The default documentation says the console command can return mixed, but something crashes elsewhere if
     *       this cannot be cast to int.
     */
    public function handle()
    {
        $name = $this->argument('name');
        $email = $this->argument('email');

        $password = $this->secret("Enter password for {$email}: ");
        $confirm = $this->secret("Confirm password: ");

        if ($password !== $confirm) {
            $this->error('Password confirmation does not match password');
        }

        User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);
    }
}
