<?php

namespace App\Console\Commands;

use App\Services\PostService;
use Illuminate\Console\Command;

class PostList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:list {limit} {offset} {--deleted}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lists all posts.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $limit = $this->argument('limit');
        $offset = $this->argument('offset');
        $deleted = $this->option('deleted');

        $postService = resolve(PostService::class);

        $headers = [ 'ID', 'Title', 'Created', 'Updated', 'State' ];

        if ($deleted) {
            $posts = $postService->getDeleted($limit, $offset);
        } else {
            $posts = $postService->getRecent($limit, $offset, true);
        }

        $table = [];
        foreach ($posts as $post) {
            $table[] = [
                $post->id,
                $post->title,
                $post->created_at,
                $post->updated_at,
                $post->state,
            ];
        }

        $this->table($headers, $table);
    }
}
