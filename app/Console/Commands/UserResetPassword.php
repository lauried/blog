<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\ResetsPasswords;

class UserResetPassword extends Command
{
    use ResetsPasswords;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:resetpassword {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resets the password for a user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        try {
            $user = User::where('email', '=', $email)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            $this->error("User does not exist");
            return 1;
        }

        $password = $this->secret("Enter password for {$email}: ");
        $confirm = $this->secret("Confirm password: ");

        if ($password !== $confirm) {
            $this->error('Password confirmation does not match password');
            return 1;
        }

        $this->resetPassword($user, $password);
    }
}
