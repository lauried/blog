<?php

namespace App\Console\Commands;

use App\Services\PostService;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PostCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:create {userEmail} {title} {content}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a post. Only really intended for testing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('userEmail');
        try {
            $user = User::where('email', '=', $email)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            $this->error("User '{$email}' does not exist");
            return 1;
        }

        $postService = resolve(PostService::class);

        $title = $this->argument('title');
        $content = $this->argument('content');

        $postService->createPost($user, $title, $content);
    }
}
