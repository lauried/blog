<?php

namespace App\Console\Commands;

use App\Post;
use App\Services\PostService;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PostDisplay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:display {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Displays a particular post.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('id');
        try {
            $post = Post::find($id);
        } catch (ModelNotFoundException $e) {
            $this->error("Post '{$id}' does not exist");
            return 1;
        }

        $this->line("id = '{$post->id}'");
        $this->line("title = '{$post->id}'");
        $this->line("content = <<<CONTENT\n{$post->text->content}\nCONTENT;");

        $postService = resolve(PostService::class);
        $linkedPosts = $postService->getLinksFromPost($post);
        foreach ($linkedPosts as $linkedPost) {
            $this->line('Links to: ' . $linkedPost->uri);
        }

        $linkingPosts = $postService->getLinksToPost($post);
        foreach ($linkingPosts as $linkingPost) {
            $this->line('Linked from: ' . $linkingPost->uri);
        }
    }
}
