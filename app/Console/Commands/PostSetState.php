<?php

namespace App\Console\Commands;

use App\Post;
use App\Services\PostService;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PostSetState extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:setstate {userEmail} {id} {state}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('userEmail');
        try {
            $user = User::where('email', '=', $email)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            $this->error("User '{$email}' does not exist");
            return 1;
        }

        $id = $this->argument('id');
        try {
            $post = Post::find($id);
        } catch (ModelNotFoundException $e) {
            $this->error("Post '{$id}' does not exist");
            return 1;
        }

        $state = $this->argument('state');
        $postService = resolve(PostService::class);
        switch ($state) {
            case Post::ACTIVE:
                $postService->showPost($user, $post->id);
                break;
            case Post::HIDDEN:
                $postService->hidePost($user, $post->id);
                break;
            case Post::DELETED:
                $postService->deletePost($user, $post->id);
                break;
            default:
                $this->error("State must be one of '" . implode("', '", Post::STATES) . "'");
        }
    }
}
