<?php

namespace App\Services;

use App\ConfigKey;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class ConfigService
 *
 * The site's config, edited through admin access on the frontend.
 * The service limits us to a single query to retrieve all config keys.
 *
 * @package App\Services
 */
class ConfigService
{
    /** @var string[] */
    private $config;

    /**
     * @param string $name
     * @param string $default
     * @return string
     */
    public function getConfig(string $name, string $default = '')
    {
        $this->ensureConfigLoaded();
        return isset($this->config[$name]) ? $this->config[$name] : $default;
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function setConfig(string $name, string $value)
    {
        try {
            $key = ConfigKey::where('key', '=', $name)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            $key = new ConfigKey();
            $key->name = $name;
        }

        $key->value = $value;
        $key->save();

        if (is_array($this->config)) {
            $this->config[$name] = $value;
        }
    }

    private function ensureConfigLoaded()
    {
        if (!is_array($this->config)) {
            $keys = ConfigKey::all();
            $config = [];
            foreach ($keys as $key) {
                $config[$key->name] = $key->value;
            }
        }
    }
}