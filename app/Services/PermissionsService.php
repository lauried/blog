<?php
/**
 * Created by PhpStorm.
 * User: ferne
 * Date: 14/02/21
 * Time: 15:13
 */

namespace App\Services;

use App\Post;
use App\User;

class PermissionsService
{
    /**
     * @param User $user
     * @param Post $post
     * @return boolean
     */
    public function userCanViewPost(User $user, Post $post)
    {
        return true;
    }
}