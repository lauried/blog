<?php

namespace App\Services;

use App\Post;
use App\PostIndex;
use App\PostLink;
use App\PostTag;
use App\PostText;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PostService
{
    /** @var PermissionsService */
    private $permissions;

    /** @var string */
    const WORD_SPLIT_REGEX = '/[^\\p{L}]+/';

    /** @var string */
    const MARKDOWN_LINK_REGEX = '/\\[(.*)\\]\\((.*)\\)/';

    /**
     * @param PermissionsService $permissions
     */
    public function __construct(PermissionsService $permissions)
    {
        $this->permissions = $permissions;
    }

    /**
     * @param User $user
     * @param string $title
     * @param string $content
     * @param int $flags
     * @return Post
     */
    public function createPost(User $user, string $title, string $content, int $flags = 0)
    {
        $text = new PostText;
        $text->content = $content;
        $text->save();

        $post = new Post;
        $post->user_id = $user->id;
        $post->title = $title;
        $post->uri = $this->makeUri($title);
        $post->post_text_id = $text->id;
        $post->flags = $flags;
        $post->save();

        $this->updateLinks($post);
        $this->updateIndex($post);

        return $post;
    }

    /**
     * @param User $user
     * @param int $postId
     * @param string $title
     * @param string $content
     * @param string|null $state
     * @param int $flags
     * @return Post
     */
    public function updatePost(
        User $user,
        int $postId,
        string $title,
        string $content,
        string $state = null,
        int $flags = 0
    ) {
        /** @var Post $post */
        $post = Post::findOrFail($postId);
        $post->title = $title;
        $post->flags = $flags;
        if ($state !== null) {
            $post->state = $state;
        }
        $post->save();

        $text = $post->text;
        $text->content = $content;
        $text->save();

        $this->updateLinks($post);
        $this->updateIndex($post);

        return $post;
    }

    /**
     * @param User $user
     * @param int $postId
     * @param string $uri
     */
    public function updatePostUri(User $user, int $postId, string $uri)
    {
        /** @var Post $post */
        $post = Post::findOrFail($postId);
        $post->uri = $this->makeUri($uri);
        $post->save();
    }

    /**
     * @param User $user
     * @param Post $post
     * @param string[] $tags
     */
    public function setTags(User $user, Post $post, array $tags)
    {
        $data = array_map(function ($tag) use ($post) { return [ 'post_id' => $post->id, 'name' => $tag ]; }, $tags);
        PostTag::where('post_id', '=', $post->id)->delete();
        PostTag::insert($data);
    }

    /**
     * @param User $user
     * @param int $postId
     */
    public function showPost(User $user, int $postId)
    {
        /** @var Post $post */
        $post = Post::findOrFail($postId);
        $post->state = Post::ACTIVE;
        $post->save();
    }

    /**
     * @param User $user
     * @param int $postId
     */
    public function hidePost(User $user, int $postId)
    {
        /** @var Post $post */
        $post = Post::findOrFail($postId);
        $post->state = Post::HIDDEN;
        $post->save();
    }

    /**
     * @param User $user
     * @param int $postId
     */
    public function deletePost(User $user, int $postId)
    {
        /** @var Post $post */
        $post = Post::findOrFail($postId);
        $post->state = Post::DELETED;
        $post->save();
    }

    /**
     * @param User|null $user
     * @return Post
     */
    public function getHomepagePosts(?User $user)
    {
        if ($user === null && !config('app.guest_mode')) {
            throw new ModelNotFoundException();
        }

        return Post::where('homepage', true)
            ->wherein('private', ($user === null) ? [ false ] : [ true, false ])
            ->firstOrFail();
    }

    /**
     * @param User|null $user
     * @param string $uri
     * @return Post
     */
    public function getPostByUri(?User $user, string $uri)
    {
        if ($user === null && !config('app.guest_mode')) {
            throw new ModelNotFoundException();
        }

        return Post::where('uri', '=', $uri)
            ->with('tags')
            ->wherein('private', ($user === null) ? [ false ] : [ true, false ])
            ->firstOrFail();
    }

    /**
     * @param User|null $user
     * @param int $limit
     * @param int $offset
     * @param string $order
     * @param string $level
     * @return Collection
     */
    public function getRecent(
        ?User $user,
        int $limit,
        int $offset,
        string $order = Post::ORDER_CREATED,
        string $level = Post::ACTIVE
    ) {
        if ($user === null && !config('app.guest_mode')) {
            return new Collection();
        }

        $states = [ Post::ACTIVE ];
        if ($user !== null) {
            $states = $this->postStatesForLevel($level);
        }

        return Post::orderBy($order, 'desc')
            ->wherein('private', ($user === null) ? [ false ] : [ true, false ])
            ->wherein('state', $states)
            ->where('nohistory', false)
            ->limit($limit)
            ->offset($offset)
            ->get();
    }

    /**
     * @param User|null $user
     * @param int $limit
     * @param int $offset
     * @return Collection
     */
    public function getDeleted(?User $user, int $limit, int $offset)
    {
        if ($user === null) {
            return new Collection();
        }

        return Post::orderBy('updated_at', 'desc')
            ->wherein('private', ($user === null) ? [ false ] : [ true, false ])
            ->where('state', Post::DELETED)
            ->limit($limit)
            ->offset($offset)
            ->get();
    }

    /**
     * @param User|null $user
     * @param string $query
     * @return Collection
     */
    public function autocomplete(?User $user, string $query)
    {
        if ($user === null && !config('app.guest_mode')) {
            return new Collection();
        }

        return Post::where('title', 'ilike', '%' . $query . '%')
            ->wherein('private', ($user === null) ? [ false ] : [ true, false ])
            ->limit(8)
            ->get();
    }

    /**
     * @param User|null $user
     * @param Post $post
     * @param string $level
     * @return Collection
     *
     * $level is one of Post::STATES.
     * If it's ACTIVE, only active posts will be returned.
     * If HIDDEN, active and hidden posts will be returned.
     * If DELETED, active, hidden and deleted posts will be returned.
     */
    public function getLinksFromPost(?User $user, Post $post, string $level = Post::ACTIVE)
    {
        if ($user === null && !config('app.guest_mode')) {
            return new Collection();
        }

        if ($user === null) {
            $level = Post::ACTIVE;
        }

        return Post::whereIn('state', $this->postStatesForLevel($level))
            ->wherein('private', ($user === null) ? [ false ] : [ true, false ])
            ->leftJoin('post_links', 'posts.id', '=', 'post_links.target_post_id')
            ->where('post_links.post_id', '=', $post->id)
            ->get();
    }

    /**
     * @param User|null $user
     * @param Post $post
     * @param string $level
     * @return Collection
     *
     * $level is one of Post::STATES.
     * If it's ACTIVE, only active posts will be returned.
     * If HIDDEN, active and hidden posts will be returned.
     * If DELETED, active, hidden and deleted posts will be returned.
     */
    public function getLinksToPost(?User $user, Post $post, string $level = Post::ACTIVE)
    {
        if ($user === null && !config('app.guest_mode')) {
            return new Collection();
        }

        if ($user === null) {
            $level = Post::ACTIVE;
        }

        return Post::whereIn('state', $this->postStatesForLevel($level))
            ->wherein('private', ($user === null) ? [ false ] : [ true, false ])
            ->leftJoin('post_links', 'posts.id', '=', 'post_links.post_id')
            ->where('post_links.target_post_id', '=', $post->id)
            ->get();
    }

    /**
     * @param User|null $user
     * @param string $query
     * @param int $offset
     * @param int $count
     * @param string $level
     * @return Collection
     */
    public function searchPosts(?User $user, string $query, int $offset, int $count, string $level = Post::ACTIVE)
    {
        if ($user === null && !config('app.guest_mode')) {
            return new Collection();
        }

        $terms = preg_split(self::WORD_SPLIT_REGEX, $query);
        if (empty($query) || empty($terms)) {
            return new Collection();
        }

        $terms = array_map('strtolower', $terms);

        if ($user === null) {
            $level = Post::ACTIVE;
        }

        $fields = [
            'title',
            'uri',
            'created_at',
            'updated_at',
            'state',
        ];

        return Post::join('post_index', 'post_id', '=', 'id')
            ->select($fields)
            ->whereIn('state', $this->postStatesForLevel($level))
            ->wherein('private', ($user === null) ? [ false ] : [ true, false ])
            ->where(function ($query) use ($terms) {
                $method = 'where';
                foreach ($terms as $term) {
                    $query->$method('post_index.word', 'LIKE', "%{$term}%");
                    $method = 'orWhere';
                }
            })
            ->groupBy($fields)
            ->orderByRaw('SUM(count) DESC')
            ->limit($count)
            ->offset($offset)
            ->get();
    }

    /**
     * @param User|null $user
     * @param string $query
     * @param string $level
     * @return Collection
     */
    public function searchTags(?User $user, string $query, string $level = Post::ACTIVE)
    {
        if ($user === null && !config('app.guest_mode')) {
            return new Collection();
        }

        if ($user === null) {
            $level = Post::ACTIVE;
        }

        return PostTag::whereHas('post', function ($query) use ($level) {
                $query->whereIn('state', $this->postStatesForLevel($level));
            })->where('name', 'like', '%' . $query . '%')
            ->orderBy('name', 'asc')
            ->groupBy('name')
            ->select('name')
            ->get();
    }

    /**
     * @param User|null $user
     * @param string $level
     * @return Collection
     */
    public function getAllTags(?User $user, string $level = Post::ACTIVE)
    {
        if ($user === null && !config('app.guest_mode')) {
            return new Collection();
        }

        if ($user === null) {
            $level = Post::ACTIVE;
        }

        return PostTag::whereHas('post', function ($query) use ($level) {
            $query->whereIn('state', $this->postStatesForLevel($level));
        })->orderBy('name', 'asc')
            ->groupBy('name')
            ->select('name')
            ->get();
    }

    /**
     * @param User|null $user
     * @param string $tag
     * @param string $offset
     * @param string $count
     * @param string $level
     * @return Collection
     */
    public function searchPostsByTag(?User $user, string $tag, string $offset, string $count, string $level = Post::ACTIVE)
    {
        if ($user === null && !config('app.guest_mode')) {
            return new Collection();
        }

        if ($user === null) {
            $level = Post::ACTIVE;
        }

        return Post::whereHas('tags', function ($query) use ($tag) {
            $query->where('name', '=', $tag);
        })->whereIn('state', $this->postStatesForLevel($level))
        ->limit($count)
        ->offset($offset)
        ->orderBy('created_at', 'desc')
        ->get();
    }

    /**
     * @param string $level
     * @return string[]
     */
    private function postStatesForLevel(string $level)
    {
        $states = [];
        switch ($level) {
            /** @noinspection PhpMissingBreakStatementInspection */
            case Post::DELETED:
                $states[] = Post::DELETED;
            /** @noinspection PhpMissingBreakStatementInspection */
            case Post::HIDDEN:
                $states[] = Post::HIDDEN;
            case Post::ACTIVE:
            default:
                $states[] = Post::ACTIVE;
                break;
        }
        return $states;
    }

    /**
     * @param string $title
     * @return string
     */
    private function makeUri(string $title)
    {
        $title = preg_replace('/\s+/', '-', $title);
        $title = strtolower($title);
        $title = preg_replace('/[^a-z0-9-]/', '', $title);

        $posts = Post::where('uri', 'like', $title . '%')->get();
        $uris = [];
        foreach ($posts as $post) {
            $uris[$post->uri] = true;
        }

        $suffix = 0;
        $newTitle = $title;
        while (isset($uris[$newTitle])) {
            $suffix += 1;
            $newTitle = $title . '-' . $suffix;
        }

        return $newTitle;
    }

    /**
     * @param Post $post
     */
    private function updateLinks(Post $post)
    {
        $matches = [];
        preg_match_all(self::MARKDOWN_LINK_REGEX, $post->text->content, $matches, PREG_SET_ORDER);

        $uris = [];
        foreach ($matches as $match) {
            $uri = $match[2];
            if (substr($uri, 0, 6) !== '/post/') {
                continue;
            }
            $uris[substr($uri, 6)] = true;
        }

        $uris = array_keys($uris);
        $posts = Post::whereIn('uri', $uris)->get();

        $data = [];
        foreach ($posts as $target) {
            $data[] = [
                'post_id' => $post->id,
                'target_post_id' => $target->id,
            ];
        }

        PostLink::where('post_id', '=', $post->id)->delete();
        PostLink::insert($data);
    }

    /**
     * @param Post $post
     */
    private function updateIndex(Post $post)
    {
        $words = [];

        $titleWords = array_map('strtolower', preg_split(self::WORD_SPLIT_REGEX, $post->title));
        foreach ($titleWords as $word) {
            // TODO: Refactor this logic into its own class.
            if (isset($words[$word])) {
                $words[$word] += 1;
            } else {
                $words[$word] = 1;
            }
        }

        $bodyWords = array_map('strtolower', preg_split(self::WORD_SPLIT_REGEX, $post->text->content));
        foreach ($bodyWords as $word) {
            if (isset($words[$word])) {
                $words[$word] += 1;
            } else {
                $words[$word] = 1;
            }
        }

        foreach ($post->tags as $postTag) {
            $word = strtolower($postTag->name);
            if (isset($words[$word])) {
                $words[$word] += 1;
            } else {
                $words[$word] = 1;
            }
        }

        $insert = [];
        $id = $post->id;
        foreach ($words as $word => $count) {
            if (empty($word)) {
                continue;
            }
            $insert[] = [
                'post_id' => $id,
                'word' => $word,
                'count' => $count,
            ];
        }

        PostIndex::where('post_id', '=', $post->id)->delete();
        PostIndex::insert($insert);
    }
}