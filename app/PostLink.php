<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostLink extends Model
{
    public function source()
    {
        return $this->belongsTo('App\Post');
    }

    public function target()
    {
        return $this->belongsTo('App\Post', 'post_target_id');
    }
}
