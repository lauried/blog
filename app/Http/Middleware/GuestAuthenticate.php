<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Contracts\Auth\Factory as Auth;

class GuestAuthenticate extends Middleware
{
    /**
     * @param Auth $auth
     */
    public function __construct(Auth $auth)
    {
        parent::__construct($auth);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param array $guards
     * @return void|null
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function authenticate($request, array $guards)
    {
        if (config('app.guest_mode')) {
            return null;
        }

        return parent::authenticate($request, $guards);
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }
}
