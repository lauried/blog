<?php

namespace App\Http\Controllers;

use App\Post;
use App\Services\PostService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class AjaxController extends Controller
{
    /** @var PostService */
    private $postService;

    /**
     * @param PostService $postService
     */
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function autocompletePost(Request $request)
    {
        $query = $request->get('query');
        $posts = $this->postService->autocomplete($query);

        $result = [];
        foreach ($posts as $post) {
            $result[] = [
                'title' => $post->title,
                'uri' => $post->uri,
            ];
        }

        // Put the shortest first, because the search token matches a bigger proportion of it.
        usort(
            $result,
            function ($a, $b) {
                return strlen($a['title']) - strlen($b['title']);
            }
        );

        return response()->json($result, 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function autoCompleteTag(Request $request)
    {
        $query = $request->get('term');
        $user = User::find(Auth::id());

        $level = Post::ACTIVE;
        if ($user !== null) {
            $level = Cookie::get('admin-view-level');
        }

        $tags = $this->postService->searchTags($user, $query, $level);

        $result = [];
        foreach ($tags as $tag) {
            $result[] = $tag->name;
        }

        return response()->json($result, 200);
    }
}
