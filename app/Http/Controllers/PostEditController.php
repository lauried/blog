<?php

namespace App\Http\Controllers;

use App\Post;
use App\Services\PostService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostEditController extends Controller
{
    /** @var PostService */
    private $postService;

    /**
     * @param PostService $postService
     */
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
        $this->middleware('auth');
    }

    public function showEditForm(Request $request)
    {
        $postId = $request->get('post-id');
        $returnUri = $request->get('return-uri');
        $post = Post::find($postId);
        $tags = [];
        foreach ($post->tags as $tag) {
            $tags[] = $tag->name;
        }
        if ($post) {
            $params = [
                'postId' => $post->id,
                'editTitle' => $post->title,
                'editContent' => $post->text->content,
                'tags' => implode(',', $tags),
                'state' => $post->state,
                'homepage' => (bool) ($post->flags & Post::HOMEPAGE),
                'noHistory' => (bool) ($post->flags & Post::NO_HISTORY),
                'private' => (bool) ($post->flags & Post::PRIVATE),
                'returnUri' => $returnUri,
            ];
        } else {
            $params = [
                'postId' => '',
                'editTitle' => '',
                'editContent' => '',
                'tags' => '',
                'state' => Post::ACTIVE,
                'homepage' => false,
                'noHistory' => false,
                'private' => false,
                'returnUri' => $returnUri,
            ];
        }

        return view('edit-post', $params);
    }

    public function handleEditForm(Request $request)
    {
        $validatedData = $request->validate([
            'post-id' => 'integer|nullable',
            'edit-title' => 'required|string|max:255',
            'edit-content' => 'required|string|max:16777215',
            'edit-state' => 'required|string|in:' . implode(',', Post::STATES),
            'edit-tags' => 'required|string',
        ]);

        $flags = 0;
        if ($request->has('edit-homepage')) {
            $flags |= Post::HOMEPAGE;
        }
        if ($request->has('edit-private')) {
            $flags |= Post::PRIVATE;
        }
        if ($request->has('edit-nohistory')) {
            $flags |= Post::NO_HISTORY;
        }

        $user = User::find(Auth::id());

        if (empty($validatedData['post-id'])) {
            $post = $this->postService->createPost(
                $user,
                $validatedData['edit-title'],
                $validatedData['edit-content'],
                $validatedData['edit-state'],
                $flags
            );
        } else {
            $post = $this->postService->updatePost(
                $user,
                $validatedData['post-id'],
                $validatedData['edit-title'],
                $validatedData['edit-content'],
                $validatedData['edit-state'],
                $flags
            );
        }

        $tags = explode(',', $validatedData['edit-tags']);
        $this->postService->setTags($user, $post, $tags);

        $returnUri = $request->post('return-uri');
        $returnUri = parse_url($returnUri,  PHP_URL_PATH);
        if (empty($returnUri)) {
            return redirect('/post/' . $post->uri);
        }

        return redirect($returnUri);
    }
}
