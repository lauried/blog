<?php

namespace App\Http\Controllers;

use App\Post;
use App\Services\PostService;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class ContentController extends Controller
{
    /** @var int */
    const PAGE_SIZE = 50;

    /** @var PostService */
    private $postService;

    /**
     * @param PostService $postService
     */
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
        $this->middleware('auth.guest');
    }

    public function showHomepage()
    {
        $user = User::find(Auth::id());
        $post = $this->postService->getHomepagePosts($user);
        $data = [
            'post' => $post,
            'links' => $this->postService->getLinksFromPost($user, $post),
            'linkingPosts' => $this->postService->getLinksToPost($user, $post),
        ];
        return view('view-post', $data);
    }

    /**
     * @param string $id
     * @return Factory|View
     */
    public function showPost(string $id)
    {
        $user = User::find(Auth::id());
        $post = $this->postService->getPostByUri($user, $id);
        $data = [
            'post' => $post,
            'links' => $this->postService->getLinksFromPost($user, $post),
            'linkingPosts' => $this->postService->getLinksToPost($user, $post),
        ];
        return view('view-post', $data);
    }

    /**
     * @param string $offset
     * @param string $count
     * @return Factory|View
     */
    public function showRecent(string $offset, string $count)
    {
        $user = User::find(Auth::id());
        $level = Post::ACTIVE;
        if ($user !== null) {
            $level = Cookie::get('admin-view-level', Post::ACTIVE);
        }
        $posts = $this->postService->getRecent($user, $count, $offset, Post::ORDER_CREATED, $level);
        $data = [
            'title' => 'Recent',
            'offset' => $offset,
            'count' => $count,
            'posts' => $posts,
            'url' => '/recent/%O/%C',
            'time' => 'created',
        ];
        return view('recent-posts', $data);
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function showModified(Request $request)
    {
        $user = User::find(Auth::id());
        $level = Post::ACTIVE;
        if ($user !== null) {
            $level = Cookie::get('admin-view-level', Post::ACTIVE);
        }
        $offset = (int) $request->get('o', 0);
        $count = (int) $request->get('c', 50);
        $posts = $this->postService->getRecent($user, $count, $offset, Post::ORDER_UPDATED, $level);
        $data = [
            'title' => 'Recently Modified',
            'offset' => $offset,
            'count' => $count,
            'posts' => $posts,
            'url' => '/modified?o=%O&c=%C',
            'time' => 'modified',
        ];
        return view('recent-posts', $data);
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function showSearch(Request $request)
    {
        $user = User::find(Auth::id());
        $level = Post::ACTIVE;
        if ($user !== null) {
            $level = Cookie::get('admin-view-level', Post::ACTIVE);
        }
        $query = (string) $request->get('q', '');
        $offset = (int) $request->get('o', 0);
        $count = (int) $request->get('c', 50);
        $posts = $this->postService->searchPosts($user, $query, $offset, $count, $level);
        $data = [
            'posts' => $posts,
            'query' => $query,
            'offset' => $offset,
            'count' => $count,
            'url' => '/search?q=' . urlencode($query) . '&o=%O&c=%C',
            'time' => 'created',
        ];
        return view('search', $data);
    }

    /**
     * @param Request $request
     * @param string $tag
     * @return Factory|View
     */
    public function showByTag(Request $request, $tag)
    {
        $user = User::find(Auth::id());
        $level = Post::ACTIVE;
        if ($user !== null) {
            $level = Cookie::get('admin-view-level', Post::ACTIVE);
        }
        $offset = (int) $request->get('o', 0);
        $count = (int) $request->get('c', 50);
        $posts = $this->postService->searchPostsByTag($user, $tag, $offset, $count, $level);
        $data = [
            'title' => "Items For '{$tag }'",
            'offset' => $offset,
            'count' => $count,
            'posts' => $posts,
            'url' => "/tag/{$tag}?o=%O&c=%C",
            'time' => 'modified',
        ];
        return view('recent-posts', $data);
    }

    /**
     * @return Factory|View
     */
    public function listTags()
    {
        $user = User::find(Auth::id());
        $level = Post::ACTIVE;
        if ($user !== null) {
            $level = Cookie::get('admin-view-level', Post::ACTIVE);
        }
        $tags = $this->postService->getAllTags($user, $level);
        $tags = array_map(function ($tag) { return $tag->name; }, $tags->all());
        sort($tags);
        $data = [
            'tags' => $tags,
        ];
        return view('tags', $data);
    }
}
