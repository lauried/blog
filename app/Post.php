<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * Posts with this flag set are listed on the homepage.
     * @var int
     */
    const HOMEPAGE = 1;

    /**
     * Posts with this flag set are only visible to logged in users.
     * @var int
     */
    const PRIVATE = 2;

    /**
     * Posts with this flag set do not show up in history searches.
     * @var int
     */
    const NO_HISTORY = 4;

    /**
     * Active posts are visible on the site.
     * @var string
     */
    const ACTIVE = 'active';

    /**
     * Hidden posts are not visible on the site, but visible to editors.
     * @var string
     */
    const HIDDEN = 'hidden';

    /**
     * Deleted posts are kept out of the main list of posts that editors see,
     * but can be retreived from another section.
     * Deleted posts should not be relied upon to keep existing.
     *
     * @var string
     */
    const DELETED = 'deleted';

    /** @var string[] */
    const STATES = [
        self::ACTIVE,
        self::HIDDEN,
        self::DELETED,
    ];

    /** @var string */
    const ORDER_CREATED = 'created_at';

    /** @var string */
    const ORDER_UPDATED = 'updated_at';

    public function getFlagsAttribute()
    {
        $flags = 0;
        $flags |= $this->homepage ? self::HOMEPAGE : 0;
        $flags |= $this->nohistory ? self::NO_HISTORY : 0;
        $flags |= $this->private ? self::PRIVATE : 0;
        return $flags;
    }

    public function setFlagsAttribute($flags)
    {
        $this->homepage = (bool) ($flags & self::HOMEPAGE);
        $this->nohistory = (bool) ($flags & self::NO_HISTORY);
        $this->private = (bool) ($flags & self::PRIVATE);
    }

    public function author()
    {
        return $this->hasOne('App\User');
    }

    public function text()
    {
        // We don't really belong to it; the post is the owner of the text.
        // It's just that the PostText doesn't have an ID back to us.
        return $this->belongsTo('App\PostText', 'post_text_id');
    }

    public function index()
    {
        return $this->hasMany('App\PostIndex', 'post_id', 'id');
    }

    public function tags()
    {
        return $this->hasMany('App\PostTag', 'post_id', 'id');
    }
}
