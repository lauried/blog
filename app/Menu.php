<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /** @var MenuItem[] */
    private $items;

    /**
     * @param MenuItem[] $items
     */
    public function setItems(array $items)
    {
        foreach ($items as $item) {
            if (!$item instanceof MenuItem) {
                throw new \InvalidArgumentException('items must be array of ' . MenuItem::class);
            }
        }

        $this->items = $items;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        if (!is_array($this->items)) {
            $this->decodeItems();
        }

        return $this->items;
    }

    /**
     * @param array $options
     * @return bool|void
     */
    public function save(array $options = [])
    {
        $this->menu = json_encode($this->items);

        parent::save();
    }

    /**
     * Sets $this->items
     */
    private function decodeItems()
    {
        $this->items = [];

        $json = json_decode($this->menu, true);
        if (!is_array($json)) {
            return;
        }

        foreach ($json as $itemData) {
            try {
                $this->items[] = new MenuItem($itemData);
            } catch (\DomainException $exception) {
                // We just don't add an item if this happens.
            }
        }
    }
}

